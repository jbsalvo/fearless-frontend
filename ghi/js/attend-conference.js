console.log("attend js loaded");

async function handleFormSubmit() {
    const attendeeForm = document.querySelector("#create-attendee-form");
    attendeeForm.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log("json data: " + json);

        // create a variable to hold "<conid>/attendees/" to put in below fetch
        const resp = await fetch("http://localhost:8001/api/attendees/", {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        });
        
        if (resp.ok) {
            const successTag = document.getElementById('success-message');
            successTag.classList.remove("d-none");

            const formTag = document.getElementById("create-attendee-form");
            formTag.classList.add("d-none");
            attendeeForm.reset();
            const newAttendee = await resp.json();
            console.log(newAttendee);
        } else {
            console.log("ERRRRROOORRR")
        }
    });

}

window.addEventListener('DOMContentLoaded', async () => {
    handleFormSubmit()
    const selectTag = document.getElementById('conference');
    
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      
      selectTag.classList.remove("d-none");
    //   selectTag.classList.add("form-select");
      document.getElementById("loading-conference-spinner").classList.add("d-none");

    }
  
  });