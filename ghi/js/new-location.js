async function handleFormSubmit() {
    const locForm = document.querySelector("#create-location-form");
    locForm.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(locForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        
        const resp = await fetch("http://localhost:8000/api/locations/", {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        });
        
        if (resp.ok) {
            locForm.reset();
            const newLocation = await resp.json();
            // console.log(newLocation);
        } else {
            console.log("ERRRRROOORRR")
        }
    });

}


document.addEventListener("DOMContentLoaded", async () =>{

handleFormSubmit();

const url = "http://localhost:8000/api/states/";
const response = await fetch(url);
if (response.ok){
    const data  = await response.json();
    const selectTag = document.getElementById('state');
    console.log(data);
    data.states.forEach(state => {
        selectTag.innerHTML += `<option value= "${state.abbreviation}">${state.name}</option>`
    });
}
// async function for button event


})


