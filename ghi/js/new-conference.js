console.log("hello world");

async function handleFormSubmit() {
    const conferenceForm = document.querySelector("#create-conference-form");
    conferenceForm.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(conferenceForm);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        
        const resp = await fetch("http://localhost:8000/api/conferences/", {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        });
        
        if (resp.ok) {
            conferenceForm.reset();
            const newConference = await resp.json();
            console.log(newConference);
        } else {
            console.log("ERRRRROOORRR")
        }
    });

}


window.addEventListener("DOMContentLoaded", async () =>{

handleFormSubmit();

const url = "http://localhost:8000/api/locations/";
const response = await fetch(url);
if (response.ok){
    const data  = await response.json();
    const selectTag = document.getElementById('location');
    console.log(data);
    data.locations.forEach(loc => {
        const hrefID = loc.href.split('/');
        const id = hrefID[3];
        // console.log(loc.href.substring(15,16));
        selectTag.innerHTML += `<option value= "${id}">${loc.name}</option>`
    });
}
// async function for button event


});
