function createCard(name, description, pictureUrl, start, end, location) {
    return `
    <div class="col-4">
        <div class="shadow p-0 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${start} - ${end}</div>
        </div>
    </div>
    `;
}

function errorAlert() {
    return `
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>There was an Error!</strong> Please check the console logs for info.
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

const url = 'http://localhost:8000/api/conferences/';
try {
    const response = await fetch(url)

    if (!response.ok){
        
        document.querySelector('.row').innerHTML += errorAlert();
        // alert(response);
        console.log(response)

    } else {
        
        const data = await response.json();

        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
        // const conference = data.conferences[0];
        // console.log(conference)
        // function to get doc.getElemByID
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const start = new Date(details.conference.starts).toLocaleDateString('en-us');
                // const start = startDate.toLocaleDateString() 
                const end = new Date(details.conference.ends).toLocaleDateString('en-us');
                // const end = endDate.toDateString();
                const location = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, start, end, location);
                const column = document.querySelector('.row');
                column.innerHTML += html;
                // console.log(details)
                // const conferenceDescription = (`${details.conference.description}\n `);
                // //   const conWeather = (`Current Weather: ${details.weather.description}, Current Temp: ${details.weather.temp}`)
                // const imgUrl = details.conference.location.picture_url;
                // //   const weatherTag = document.querySelector('.card-extra')
                // const descTag = document.querySelector('.card-text');
                // const imageTag = document.querySelector('.card-img-top');
                // //   weatherTag.innerHTML = conWeather;
                // imageTag.src = imgUrl;
                // descTag.innerHTML = conferenceDescription;
                
            }
        }
        // function conferences = data.results;
        // fucntion to clear html if needed
        //     conf.foreEach(confer => {
            //  console.log(conference);
            // functions to filter name text and pic
            // function to inject in html using innerHTML +=

        // })
    }
} catch (e) {
    document.querySelector('.row').innerHTML += errorAlert();
    // alert(e)
    console.log('ERRRRRRRRRRROOOORRRRR')
    console.log(e);
}

});


// window.addEventListener('DOMContentLoaded', () =>{
//     console.log('dom loaded');
//     func to run
//      make button function w/ event listener 
// })
