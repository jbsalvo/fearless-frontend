import React from "react";

class ConferenceForm extends React.Component {

    constructor(props){
        super(props)
        this.state = { 
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handlesMaxPresentationsChange = this.handlesMaxPresentationsChange.bind(this);
        this.handlesMaxAttendeesChange = this.handlesMaxAttendeesChange.bind(this);
        this.handlesLocationChange = this.handlesLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        // console.log("submit data: ", data);
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        
        const locationURL = "http://localhost:8000/api/conferences/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(locationURL, fetchConfig);
        if (response.ok){
            const newConference = await response.json();
            // console.log(newConference);

        }
    }
    

    handleNameChange(e){
        const value = e.target.value;
        this.setState({name: value})
    }

    handleStartDateChange(e){
        const value = e.target.value;
        this.setState({starts:value})
    }

    handleEndDateChange(e){
        const value = e.target.value;
        this.setState({ends:value})
    }

    handleDescriptionChange(e){
        const value = e.target.value;
        this.setState({description:value})
    }

    handlesMaxPresentationsChange(e){
        const value = e.target.value;
        this.setState({maxPresentations:value})
    }

    handlesMaxAttendeesChange(e){
        const value = e.target.value;
        this.setState({maxAttendees:value})
    }

    handlesLocationChange(e){
        const value = e.target.value;
        this.setState({location:value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            // const selectTag = document.getElementById('state');
            // data.states.forEach(state => {
            // selectTag.innerHTML += `<option value= "${state.abbreviation}">${state.name}</option>`
            // });
            // console.log("conForm data: ", data);
            this.setState({locations: data.locations});

        }
      }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
    
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartDateChange} type="date" name= "starts" id="start" className="form-control" value={this.state.starts}/>
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndDateChange} type="date" name= "ends" id="end" className="form-control" value={this.state.ends}/>
                    <label htmlFor="ends">Starts</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={this.handleDescriptionChange} className="form-control" name="description" id="description" rows="3" value={this.state.description}></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlesMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name= "max_presentations" id="max_presentations" className="form-control" value={this.state.maxPresentations}/>
                    <label htmlFor="max_presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlesMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name= "max_attendees" id="max_attendees" className="form-control" value={this.state.maxAttendees}/>
                    <label htmlFor="max_attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handlesLocationChange} required id="location"  name="location" className="form-select" value={this.state.location}>
                      <option value="">Choose a location</option>
                      {this.state.locations.map(loc => {
                        const hrefID = loc.href.split('/');
                        const id = hrefID[3];
                        return (
                            <option key={id} value={id}>
                                {loc.name}
                            </option>
                        );
                      })}
                      
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
    
              </div>
            </div>
          </div>
        )
    }

}

export default ConferenceForm;