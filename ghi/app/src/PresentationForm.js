import React from "react";

class PresentationForm extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            presenterName: "",
            presenterEmail: "",
            companyName: "",
            title: "",
            synopsis: "",
            conferences: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleCompanyName = this.handleCompanyName.bind(this);
        this.handleTitle = this.handleTitle.bind(this);
        this.handleSynopsis = this.handleSynopsis.bind(this);
        this.handleConference = this.handleConference.bind(this);

        
    }

    async handleSubmit(e){
        e.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName
        data.presenter_email = data.presenterEmail
        data.company_name = data.companyName
        delete data.presenterName
        delete data.presenterEmail
        delete data.companyName 
        delete data.conferences;

        const conId = data.conference;
        const hrefID = conId.split('/');
        const id = hrefID[3];
        const url = `http://localhost:8000/api/conferences/${id}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        console.log("submitted data: ", data);
        if (response.ok){

            const newPresentation = await response.json();
            console.log(newPresentation);
            const cleared = {
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conference: '',
            };
            this.setState(cleared);
        }
    }
    
    
    async componentDidMount() {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences});
        }
    }

    
    handleName(e){
        const value = e.target.value;
        this.setState({presenterName: value})
    }
    handleEmail(e){
        const value = e.target.value;
        this.setState({presenterEmail: value})
    }
    handleCompanyName(e){
        const value = e.target.value;
        this.setState({companyName: value})
    }
    handleTitle(e){
        const value = e.target.value;
        this.setState({title: value})
    }
    handleSynopsis(e){
        const value = e.target.value;
        this.setState({synopsis: value})
    }
    handleConference(e){
        const value = e.target.value;
        this.setState({conference: value})
    }


    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>

                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={this.state.presenterName}/>
                    <label htmlFor="presenter_name">Presenter name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" value={this.state.presenterEmail}/>
                    <label htmlFor="presenter_email">Presenter email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCompanyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" value={this.state.companyName}/>
                    <label htmlFor="company_name">Company name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={this.state.title}/>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={this.handleSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" value={this.state.synopsis}></textarea>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleConference} required name="conference" id="conference" className="form-select" value={this.state.conference}>
                      <option value="">Choose a conference</option>
                      {this.state.conferences.map(con => {
                            return (
                                <option key={con.href} value={con.href}>
                                    {con.name}
                                </option>
                            )
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }




}

export default PresentationForm;
